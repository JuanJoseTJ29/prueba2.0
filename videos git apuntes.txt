-video 7:

-pwd( me muestra la carpeta donde estoy)
-cd / (para ir al home
-cd users (para ir a la caroeta users)
-ls (me muestra la lista de archivos de ese espacio)
-ls -al (me muestra la lista incluido los archivos ocultos)
-ls -l (me muestra los archivos pero no los ocultos)
-ls -a(me muestra el grupo de archivos pero no en una lista)  
- clear(para limpiar la consola o tambien puede ser ctrl L)
- cd ..(para volver a la carpeta anterior)
-mkdir proyectoJuan1 (para crear una carpeta con el nombre proyectoJuan1)
-touch vacio.text (para crear archivos con nada por dentro con el nombre de vacio y extension txt)
- cat vacio.txt (te muestra rapidamente el contenido del archivo vacio.txt)
-history(para ver el historial de comandos escritos)
- !529 (se invoca el comando numero 529)
- rm vacio.txt(para borrar el archivo.txt)
- rm --help (te muestra todos los posible comandos con rm , esto funciona para todo los commandos como ls , etc.)

Video 8:

-con git add. los cambios llegan al stagin (que es un area de preparacion) con commit esos cambios pasan de estar trackeados en stagin
a estar trackeados en el repositorio.

- Con checkout traemos los ultimos cambios o los cambios que tu quieras hacia tu carpeta local
- Cada commit es una nueva version de cambios de tu archivo hacia el repositorio

video 9:
 - merge (unes los cambios de una rama con otra)

- rama development( se le llama asi a la rama donde se va a experimentar)

-rama hotfix (se le pone ese nombre porque son los cambios que tienen que salir ya mismo)

video 10 :

-git init (para crear un repositirio)

-code (para abrir visual studio code)
- git status (el estado del proyecto de momento)
- git rm --cached hitoria.txt (irve para volver al estado antes de utilizar el add)
-git config(para encontrar todas las configuraciones que tiene git)
- git config --list(me muestra los datos por defecto)
- git config --list --show-origin(para ver donde estan las configuraciones guardadas)
- git config --global(para cambiar todos los usuarios globales o los datos)
ejem:
git config --global user.name "Juan Tirado" (Para cambiar el nombre) 
git config --global user.email "juan.tirado@unmsm.edu.pe" (Para cambiar el correo) 

-git log historia.txt(para ver el historial del archivo

-commit f9d650fb62ba8b2ed18407d6449b0de6b4ecd7e5 (HEAD -> master) (Cuando dice Head-> master  se refiere a que es el ultimo commit o el mas reciente)

Video 11:

- git show historia.txt (me muestra los cambios que han existido de un archivo en este caso el archivo historia.txt)
- Esc + shift + zz (para salir cuando no pusiste mensaje en el commmit)
- git diff cb353d3b13fa60e729a18b9e28f883acaa0bffdc 28dfb6ed069f6eb886480010de07d3205d951531 (sirve para comparar commits)

video 12: 
- git reset 4470a09027c962faf1dd6c8ddb8227ee3698d1f6(sirve para volver a una version anterior)
Hay 2 tipos de git reset:

git reset 4470a09027c962faf1dd6c8ddb8227ee3698d1f6 --hard (permitira que todo vuelva al estado anterior, es el que mas comunmente se usa pero tambien es el mas riesgoso ya que al hacer esto borras todo lo que habias hecho despues del commit al que has decidido volver)
git reset 4470a09027c962faf1dd6c8ddb8227ee3698d1f6 --soft( se vuelve a la version anterior pero lo que esta en stagin sigue en stagin es decir si has hecho git add . eso sigue ahi)

- cuando tengo cosas distinta en el directorio de trabajo, en Stagin o area de preparacion y en el repositorio (podemos usar git diff  y me muestra los cambios del director actual y del stagin)
- git log --stat (se observan los cambios especificos que se hicieron en los archivos a partir del commit)

- Con la letra q se sale cuando pones para bajar y subir
- git checkout 28dfb6ed069f6eb886480010de07d3205d951531 historia.txt (para ver como era un archivo pasado en este caso sera historia.txt) 
- git checkout master historia.txt(para que me traiga o ponga la version de master)

Video 13: 
- Resumen de todo lo que se ha visto hast ahora
- git clone url (Cuando queremos traer datos de un servidor remoto en url va el url del repositorio  remoto. Se
traera el archivo a 2 lugares, se trae una copia del master a tu directorio de trabajo y crea la base de datos de todos los 
cambios historicos en el repositorio local)

-git push (envio del repositorio local al repositorio remoto)

- git fetch (quiero traer un cambio del repositorio remoto al repositorio local , pero no me lo copia en mis archivos para eso tengo que fusionar la ultima
version que esta en mi repositorio local mi version actual)

- git marge ( fusionar la ultima version que esta en mi repositorio local con mi version actual)

-git pull (Es la combnacion de git fetch con git marge , este comando copiara el repositorio local, la base de datos de cambios y copia el directorio de esta manera siempre tenemos una copia actualizada de lo ultimo que paso en el repositorio)

Video 14 :

- El commit mas reciente se llama Head
- git commit -am (automaticamemte me hace el git add . de los cambios, solo funciona con archivos donde ya he utilizado git add . previamente) 
- git commit -a (me manda a un entorno donde puedo escribir un mensaje)
- git branch cabecera (para crear la rama cabecera)
- git checkout cabecera(me muevo a la rama cabecera)
- Para escribir cuando le das al git commit se pone Esc + i 

Video 15:

-Si hago checkout master sin guaradar los cambios de la rama se perderan estos cambios.
- Es recomendable hacer el marge enmaster
- git branch(te indica en que rama estas)
- git merge cabecera (para unir la rama master con la rama cabecera)

Video 16 :

-git mmarge master(para traerse los cambios del master a una rama)
- Para que se complete el merge tiene que ocurrir un commit

Cuando ocurre conflictos el visual studio code te da las siguientes opciones :
- Accept Current Change(Quiere aceptar el cambio actual es decir como esta en el Head)
-Accept incoming change(Quiere aceptar el cambio que viene)

Video 17:
*Dentro de GitHub:
-New organizacion(Una organizacion se refiere a que es como tu empresa)
-New project(Un grupo de repositorios que puedes tener dentro de una empresa o no necesariamente)
-New gist(Es un pedazo peque�o de codigo que puedes compartir)
-Import repository(Lo que hace es traer repositorios de otro sistema como por ejemplo GitLab )
-New repository(Para crear nuevo repositorio)
-Readme(Es un archivo que de texto se va a ver en el instante que una persona entre al repositorio en github se usa para explicar de que es el repositorio)
* Dentro de un repositorio de GitHub en el archivo README.md:
-Raw(Muestra el codigo plano que creo el texto )
-Blame(Se ve quien ha hecho cada cosa)
-History(La historia del archivo es como hacer un git log como en el git bash)
*Code(Es el boton verde de donde copiaremos el HTTPS para poder clonar, para ejecutar la clonacion vamos al git bash)
- git remote add origin https://github.com/JuanJoseTJ29/HyperUltraBlog.git (Sirve para decirle a gt que vamos a agregar un origen remoto de nuestros archivos)
-git remote (Nos muestra el origin creado)
-git remote -v (Nos muestra que tenemos un origin para hacer fetch es decir traernos cosas ; y tenemos un origin para hacer push es decir para enviar cosas )
-git push origin master(Le estamos diciendo que le envie al origen que esta en GitHub, la rama master)
-git pull origin master(Vamos a traer desde el origen en GitHb a la rama master)
-git pull origin master --allow-unrelated-histories(Permite fucionar lo que estaba en GitHub con la rama que tenemos en local )

Video 18:
-llaves publicas y llaves privadas( o cifrado asimetrico de un solo camino)
-Lo que yo cifre con la llave publica solo lo abre a llave privada
-Yo mando la llave publica(es como u proceso matematico que me permite convertir el mensaje en algo completamente oculto)
-Lo importante es que nadie conozco tu llave privada.
- Lo que haremos sera copiar un version personal de la llave publica que tomamos de algun lugar y hacemos un proceso matematico(cifrar el mensaje con la llave publica que yo genere y a partir de este proceso se genera un nuevo mensaje)
- Al pasar el mensaje secreto por la llave publica queda algo completamente imposible de decifrar, es decir queda un mensaje cifrado.
-Nosotros obtenemos el mensaje , copiando el mensaje que mensaje que llega de internet ya sea por correo o por cualquier otro metodo y usasmos la llave privada para convertirlo de regreso en las palabras que te enviarion tal cual y asi es como funcionan las llaves publicas y privadas)
- Cuando se comparten 2 llaves publicas y nosotros mantenemos las llaves privadas esto es lo que permite a 2 personas completamente tener comunicaciones seguras sin que nadie las intercepte. 

Video 19:
-Es preferible escribir el comando de la llave en el home
- ssh-keygen -t rsa -b 4096 -C "juan.tirado@unmsm.edu.pe"(para empezar a generar la llave publica y privada)
- cada laptop o pc tiene una llave distinta y unica, es recomendable crear una llave privada y una llavepublicapor cada computadora que uses .
- eval $(ssh-agent -s)(para ver si las llaves estan corriendo):
Aparecera Agente pid 4724 , esto significa:
Agent(significa que el servidor de ssh esta corriendo)
pid(es el porcess id o el identificandor del proceso)
4724(el numero es diferente para cada persona y significa que para el sistema operativo significa que el proceso esta corriendo)

-ssh-add ~/.ssh/id_rsa(para agregar la llave que acabamos de crear)
id_rsa(llave privada) id_rsa.pub(llave publica)
se agrega en el video del curso la privada

video 20:

-En GitHub ir a settings y de ahi ha SSH and GPG keys para agregar la lave publica .
-git remote -v (nos mostrara la url de nuestros repositorio)
-git remote set-url origin git@github.com:JuanJoseTJ29/HyperUltraBlog.git (para cambiar el url de origin. El nuevo url lo sacamos de GitHub especificamente del boton verde code y la opcion ssh )
- Traemos primero todo los cambios con git pull, nos arrojara un mensaje sobre la autentificacion le damos yes.
- git pull origin master( se fusionara  origin que es nombre de nuestro repositorio remoto con la rama actual que es master, en general todo siempre va estar actualizado esto se hace mas por que es una buena practica )
-git diff (me va decir que es lo que cambio)
- git commit -am "Una version del HyperUltraBlog"
-Despues del commit es recomendable hacer nuevamente el git pull origin master
-git push origin master(para enviarlo a repositorio de GitHub) 

Video 21 :

-git log --all --graph (para que te muestre las ramas graficamente)
-git log --all --graph --decorate --oneline(te mostrara todo graficamente mas comprimido)
-alias arbolito "git log --all --graph --decorate --oneline"(Servira para poder ejecutar todo ese comando poniendo la palabra arbolito)
- git tag -a v0.1 -m "Resultado de las primeras clases del curso" da2f250(Para crear un tag , da2f250 es el commit)
- git tag(para ver los tag que tengo)
- git show-ref --tags(Para saber a que hasht o commit esta conectado el tag)
- los tags no son cambios asi que al poner git status me aparecera que no falta nada enviar aparentemente
ya que en realidad si hay algo que enviar , ya que la idea es que los tags los pueda ver alguien es decir seran
utiles en GitHub 
-git pull origin master (para traernos todo lo que esta en el github)
-git push origin --tags (Para enviar los tags que creamos al repositorio en github)
- Una vez hecho lo anterior en el github en la parte de branch estara la opcion de tags que nos mostrara los que tenemos y habiamos enviado.
-git tag -d dormido (para eliminar un tag que en este caso tiene el nombre de dormido, de nuestro repositorio local)
- git push origin :refs/tags/dormido (Para eliminar el tag dormido en el github)

Video 22:

- git show-branch(Me muestra las ramas que existe y cual ha sido su historia)
- git show-branch --all(Parecido al comando anterior pero te da un poco mas de informacion )
-gitk (Te abre en un software la historia de las ramas en una manera visual)
- git checkout cabecera
- git push origin cabecera(Vamos a empujarle al orign que es la rama en internet , la rama cabecera osea vamos a hacer que aparezca la rama cabecera en el github)
- git checkout master
- crearemos nuestras ramas header y footer con el comando git branch header y git brnach fooder 
-git push origin header
-git push origin footer
- y con los 2 comandos anteriores tendriamos nnuestras 2 nuevas ramas en GitHub 

Video 23: 

-El coloborador no va a hacer un git init por que lo que quiere es traerse de github un repositorio
-git clone https://.....       (para clonar el repositorio)
- cambiara el archivo historia.txt


Video 24: 

-Github no es muy preciso con las imagenes ya que son archivos binarios (ctrl + shift + r para forzar la actualizacion)

video 25:

-git pull origin footer (pra traerme la rama footer)

-Para hacer el merge tenemos que ir a la rama master

-git merge header(para unir la rama header a la rama master)

-cuando un extra�o es que hace los cambios normalmente a esto se le llama pull request

video 26:

-El pull request en si  describe lo que esta pasando

- un pull request puede ser un commit como varios commits

- request changes(para en la revision no aceptar el pull request)

-Que los cambios este aprovados no significa que el merge se haya ejecutado

- Un pull request es como pausa justo antes de fusionar, es como trabajar en un stagin del lado del servidor que te permite agregar cambios.


Video 27:

-Al quitarle de colaborador del proyecto a alguien, este ya no puede hacer push , crear ramas, hacer merge , lo unico que puede hacer es clonar el repositorio.
-La opcion fork sirver para clonar el proyecto en el mismo github y puedo ver los cambios hechos

- para clonar el repositorio a tu pc es decir a travez de git tienes que  estar en el fork del  proyecto en github.
git clone https://github......
- Subir bien los cacmbios que se han hecho antes de traerte los que estan en github
-git remote add upstream https://..... (Nos crea otra fuente nueva llamada upstream de donde podemos traer datos a nuestra rama master, el link tienes que copiarlo del repositorio original)
-git pull upstream master (nos traeremos todos los cambios del master)
- git commit -am "fusion"
-git push origin master (para enviarlo al repositorio original) 


Video 28: 

-Como asociar tu pagina web o tu servidor con github o hacer un deployment

video 29:

-reto de hacer pull request

video 30:

- .gitignore(es ua lista de los archivos que vamos a ignorar)
-En imgur.com(puedes subir gratis imagenes y referenciarlas desde ahi de esa manera tendremos un lugar externo donde la imagen nos funciona, sobretodo agregarlo sin necesidad de tenerlo en el repositorio, asi podremos refernciar imaganes de otro lado en nuestro propio sistema )

video 31:

-Readme .md:
md significa mark down es una especie de codigo que te permiten cambiar ligeramente la forma en que se ve un archivo de texto.

- editor.md (es un editor online)

vide 32:

- github page (pagina web con github)

video 33: 
-rebase solo para repositorios locales
-rebase reescribe la historia del repositorio 

-dentro de la rama donde estan los cambios hacemos un :
git rebase master

- primero se hace el rebase a la rama que voy a desaparecer de la historia o la rama que ha cambiado y luuego se hace rebase a la rama principal.
- git branch -D experimento(para borrar la rama experimento)
- un rebase es una forma de hacer cambios silenciosos en otras ramas y volver a pegar la historia de esa rama a una rama anterior.
-El problema con rebase es que no queda historia , no se sabe realmente quien hizo que y en ocasiones si la rama master a avanzado mucho puede crear muchos conflictos que necesitan ser arreglados de manera manual.